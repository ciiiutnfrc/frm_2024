# Trabajos prácticos
En esta página estarán listados los Trabajos prácticos a realizar.

* [TP0: Programación a bajo nivel de un robot móvil de tracción diferencial](2024_TP0.md)
* [TP1: Modelo cinemático y odometría en robot de tracción diferencial](2024_TP1.md)
* [TP2: Modelo dinámico de un multirrotor de un grado de libertad](2024_TP2.md)
* [TP3: Mediciones de sensor de barrido láser](2024_TP3.md)
