import os

import rclpy
from rclpy.node import Node

from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from tf_transformations import euler_from_quaternion
from rosbag2_interfaces.srv import Resume

class OdomScanLog(Node):
    """ Node class """
    def __init__(self):
        """ Class constructor """
        super().__init__('odom_scan_log')

        # Node parameters (ToDo: convert to parameters)
        n_dig = 5
        odom_filename = 'odom.txt'
        scan_filename = 'scan.txt'
        to_wait = ['/odom', '/scan']

        self.declare_parameter('out_dir', os.environ['HOME'])
        out_dir = self.get_parameter('out_dir').get_parameter_value().string_value

        n_wait = len(to_wait)
        self.format = '{:.' + str(n_dig) + 'f}' # '{:.5f}'.format(num)
        self.get_logger().info('Topics to wait: {}'.format(to_wait))

        # Text files
        odom_filename = out_dir + '/' + odom_filename
        scan_filename = out_dir + '/' + scan_filename

        # Waiting for needed topics
        while len(to_wait) > 0 and rclpy.ok():
            self.get_logger().info('Waiting {}/{} topics ({})'.format(
                len(to_wait), n_wait, to_wait))
            names_and_types = self.get_topic_names_and_types()
            current_topics = list(zip(*names_and_types))[0]

            for wait_topic in to_wait:
                if wait_topic in current_topics:
                    to_wait.remove(wait_topic)

        # Node subscribers
        odom_sub = self.create_subscription(
            Odometry,
            'odom',
            self.odom_cb,
            10)

        scan_sub = self.create_subscription(
            LaserScan,
            'scan',
            self.scan_cb,
            10)

        # Node service clients
        self.cli_srv = self.create_client(Resume, 'rosbag2_player/resume')
        while not self.cli_srv.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("Waiting '{}' service".format(self.cli_srv.srv_name))

        # Open file for writing                                                       
        self.odom_fd = open(odom_filename, 'w')                                       
        self.scan_fd = open(scan_filename, 'w')

        # Resume rosbag play
        self.get_logger().info('Starting rosbag playback...')
        self.cli_srv.call_async(Resume.Request())

        # Ready...
        self.get_logger().info('Writing files...')
        self.get_logger().info(' - ' + odom_filename)
        self.get_logger().info(' - ' + scan_filename)

    def odom_cb(self, msg):
        """ Odometry subcriber callback """
        #self.get_logger().info('odom_cb')

        # Quaternion to yaw angle (RPY)
        quat = msg.pose.pose.orientation
        orientation_list = [quat.x, quat.y, quat.z, quat.w]
        (_,_,yaw) = euler_from_quaternion(orientation_list)

        timestamp = msg.header.stamp.sec + msg.header.stamp.nanosec * 1e-9
        line = self.format.format(timestamp) + '\t'
        line += self.format.format(msg.pose.pose.position.x) + '\t'
        line += self.format.format(msg.pose.pose.position.y) + '\t'
        line += self.format.format(yaw) + '\t'
        line += self.format.format(msg.twist.twist.linear.x) + '\t'
        line += self.format.format(msg.twist.twist.angular.z) + '\n'
        self.odom_fd.write(line)
        #self.get_logger().info(line)

    def scan_cb(self, msg):
        """ Laser scan subcriber callback """
        #self.get_logger().info('scan_cb')

        timestamp = msg.header.stamp.sec + msg.header.stamp.nanosec * 1e-9
        line = self.format.format(timestamp) + '\t'
        for r in msg.ranges:
            line += self.format.format(r) + '\t'
        line = line[:-1] + '\n'
        self.scan_fd.write(line)
        #self.get_logger().info(line)

def main(args=None):
    """ Node main function """
    rclpy.init(args=args)

    try:
        odom_scan_log = OdomScanLog()
        rclpy.spin(odom_scan_log)
    except KeyboardInterrupt:
        pass

    # Closing files if opened
    try:
        if not odom_scan_log.odom_fd.closed:
            odom_scan_log.odom_fd.close()
        if not odom_scan_log.scan_fd.closed:
            odom_scan_log.scan_fd.close()
    except UnboundLocalError:
        pass

    rclpy.try_shutdown()

if __name__ == '__main__':
    main()
