"""
ROS node for 2D odometry dump
"""

import rclpy
from rclpy.node import Node

from nav_msgs.msg import Odometry
from tf_transformations import euler_from_quaternion

class DumpOdom(Node):
    """ Node class """
    def __init__(self):
        """ Class constructor """
        super().__init__('dump_odom')

        # Node subscribers
        self.odom_sub = self.create_subscription(
            Odometry,
            'odom',
            self.odom_cb,
            10)
        self.odom_sub       # prevent unused variable warnings

    def odom_cb(self, msg):
        """ Odometry subcriber callback """
        #self.get_logger().info('odom_cb')

        # Quaternion to yaw angle (RPY)
        quat = msg.pose.pose.orientation
        orientation_list = [quat.x, quat.y, quat.z, quat.w]
        (_,_,yaw) = euler_from_quaternion(orientation_list)

        print(
            str(self.get_clock().now().nanoseconds) + '\t' + \
            str(msg.pose.pose.position.x) + '\t' + \
            str(msg.pose.pose.position.y) + '\t' + str(yaw) + '\t' +\
            str(msg.twist.twist.linear.x) + '\t' +\
            str(msg.twist.twist.angular.z))

def main(args=None):
    """ Node main function """
    rclpy.init(args=args)
    try:
        dump_odom = DumpOdom()
        rclpy.spin(dump_odom)
    except KeyboardInterrupt:
        pass
    finally:
        rclpy.try_shutdown()
        dump_odom.destroy_node()
    #rclpy.shutdown()

if __name__ == '__main__':
    main()
