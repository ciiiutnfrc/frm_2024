import threading

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Twist

class SquarePath(Node):

    def __init__(self):
        super().__init__('square_path')

        # Publicador del nodo
        self.vel_pub = self.create_publisher(Twist, 'cmd_vel', 10)

        # Variables de velocidad
        self.twist_v = Twist()

        # Spin in a separate thread
        thread = threading.Thread(target=rclpy.spin, args=(self, ), daemon=True)
        thread.start()

        self.rate_freq = 10.0
        self.rate = self.create_rate(self.rate_freq)

        # Espera a que se establezca la conexión
        n_sub = self.vel_pub.get_subscription_count()
        self.get_logger().info('n_sub: {}'.format(n_sub))
        while n_sub == 0:
            self.get_logger().info('Esperando conexión...')
            self.rate.sleep()
            n_sub = self.vel_pub.get_subscription_count()

        self.get_logger().info('Ejecutando camino...')
        
        # Ejecuta el camino
        try:
            while rclpy.ok():
                # Avance - Fijar velocidad, publicar y esperar
                # Las esperas o delay se realizan con:
                # self.rate.sleep()

                #self.twist_v.linear.x = 0.1
                #self.twist_v.angular.z = 0.0
                #self.vel_pub.publish(self.twist_v)
                
        except KeyboardInterrupt:
            pass

def main(args=None):
    rclpy.init(args=args)
    square_path = SquarePath()
    rclpy.spin(square_path)

if __name__ == '__main__':
    main()
