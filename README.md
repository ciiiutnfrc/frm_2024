# Fundamentos de robótica móvil 2024

Repositorio de la materia electiva _"Fundamentos de Robótica Móvil"_ de la Carrera de Ingeniería Elctrónica de la Facultad Regional Córdoba, Universidad Tecnólogica Nacional (UTN-FRC).

Página oficial de la materia:

https://www.profesores.frc.utn.edu.ar/electronica/fundamentosroboticamovil/

En este repositorio encontrará las guías de ejercicios y enunciados de los trabajos prácticos.

## Contenido
* [Guías de ejercicios](guias/README.md)
* [Trabajos prácticos](tps/README.md)
