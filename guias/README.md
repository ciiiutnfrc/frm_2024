# Guías de ejercicios

Las guías de ejercicio a resolver son:
* [Introducción a Python](GuiaPython.md)
* [Gráficas con Python](GuiaPythonPlot.ipynb)
* [Sistema Operativo de Robótica - ROS](GuiaROS2.md)

Material de consulta:
* [Introducción a Python](intro_python) (Notebooks con intro a Python)
* [Introducción a Python de Federico Cerisola](https://materias.df.uba.ar/fluidosa2017c2/files/2017/08/python_intro.pdf)
