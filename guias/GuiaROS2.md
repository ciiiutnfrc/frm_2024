# Guía de ejercicios: El Sistema Operativo de Robótica (ROS2)

## Puesta en marcha de ROS Development Studio (ROSDS)

  1. Iniciar sesión en ROSDS: [https://www.theconstructsim.com/](https://www.theconstructsim.com/).
  1. En el panel izquierdo ir a "My Rojects".
  1. Crear nuevo proyecto (ROSject), eligiendo "ROS2 Humble" como distribución (ROS Distro) a utilizar.
  1. Ejecutar el proyecto.
  1. Abrir una ventana de la Shell (Web Shell).

## Inspeccionar el valor de variables de entorno usadas por ROS

  * Versión de ROS:
      ```bash
      echo $ROS_VERSION
      ```
  * Distribución de ROS:
      ```bash
      echo $ROS_DISTRO
      ```
  * Ruta para buscar los paquetes:
      ```bash
      echo $AMENT_PREFIX_PATH
      ```
      O bien: `echo $AMENT_PREFIX_PATH | tr ":" "\n"`.
  * Ver todas las variables de entorno ROS:
      ```bash
      printenv | grep -i ROS
      ```

## Simulación del robot TurtleBot3 en el simulador Gazebo

ROSDS incluye los paquetes necesarios para la simulación del robot TurtleBot3.
En caso de queres realizar la instalación se puede seguir el siguiente enlace: [https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/](https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/))

1. Lanzar la simulación utilizando el modelo "burger" del robot:
    ```
    > export TURTLEBOT3_MODEL=burger
    > ros2 launch turtlebot3_gazebo empty_world.launch.py
    ```
1. Ejecutar el nodo de teleoperación mediante teclado:
    ```
    > export TURTLEBOT3_MODEL=burger
    > ros2 run turtlebot3_teleop teleop_keyboard
    ```
    Mover el robot utilizando el teclado como lo indica la aplicación.
1. Detener (`Ctrl+C`) el nodo de teleopearción y ejecutar:
    ```
    > ros2 topic pub -r 1 /cmd_vel geometry_msgs/msg/Twist "{linear: {x: 0.2, y: 0.0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 0.2}}"
    ```
1. Detener (`Ctrl+C`) el simulador Gazebo de la primer Shell y lanzar:
    ```
    > ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
    ```
    Volver a lanzar el nodo de teleoperación y comandar el robot mediante el teclado.



### Configurar por defecto el modelo "burger"
Para utilizar de forma permanente el modelo "burger" del robot sin tener que ejecutar el comando `export` en cada Shell, ejecutar:
```
> echo "export TURTLEBOT3_MODEL=burger" >> ~/.bashrc
```
(para aplicar la configuración en la Shell actual: `> source ~/.bashrc`)

## Ejercicios de línea de comando de ROS
Realizar las siguientes actividades teniendo en ejecución el simulador Gazebo con el modelo del robot TurtleBot3.

1. Utilizar el comando de ROS para listar los nodos en ejecución, ¿cuáles son?

1. Utilizar el comando de ROS para listar los tópicos disponibles, ¿cuáles son?

1. ¿Qué tipo de mensaje lleva el tópico `odom`?

1. ¿Cuáles son los campos que componen el mensaje del tópico `odom`?

1. Ejecutar el comando de ROS que permite ver los mensajes publicados en el tópico `odom`.

1. Ejecutar el comando de ROS para solo la pose (posición y orientación) del robot dada en el tópico `odom`o

1. Ejecutar el comando de ROS que permite determinar la frecuencia a la que se publican los mensajes del tópico `odom`.

1. Ejecutar el comando que permita ver el campo `position` del tópico `odom` (posición x, y y z).

1. ¿Qué tipo de mensaje lleva el tópico `scan`?

1. ¿Cuáles son los campos que componen el mensaje del tópico `scan`?

1. Ejecutar el comando de ROS que permite ver los mensajes publicados en el tópico `scan`.

1. Ejecutar `topic echo` con las ociones `--once` y `--no-arr`, ¿cuáles son los parámetros del barrido láser?, ¿cuántos valores se incluyen en el campo `range`?
