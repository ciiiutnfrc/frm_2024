# Ejercicios sobre Python

Utilizar la terminal interactiva de Python (`ipython`) para realizar las siguientes actividades:

  1. Definir diferentes variables de tipo `int`, `float` y `string` y mostrar sus valores en la terminal interactiva y utilizando la función `print`.

  1. ¿Qué acciones realizan las funciones `type()` y `dir()`? (Ver ayuda con el comando `type?` y `dir?`).

  1. ¿Qué tipo de dato devuelve al ejecutar la sentencia?
      ```python
      dir(list())
      ```
      ¿Qué significado tiene su contenido?

  1. Buscar información sobre las 3 maneras de formatear una cadena: i)_"printf-stype"_ con `%s`, ii) usando `.format()` y iii) con _"f-string"_.

  1. Utilizar el módulo matemático (`math`) para imprimir el valor de la constante $`\pi`$. Mostrar su valor con 3, 6 y 10 decimales utilizando el formateo de cadenas `.format()` y _"f-string"_.

  1. ¿Cuáles son los métodos del tipo de datos `str()`.

  1. ¿Qué hacen los siguientes métodos de una lista: `append()`, `clear()`, `count()`, `index()`. ¿Cómo se puede agregar y eliminar un elemento del medio?

  1. ¿Qué hace el slice al aplicarlo sobre una lista `my_list`como `my_list[:]`?

  1. ¿Qué devuelve la ejecución de `dir(tuple())` y qué contiene?

  1. Crear una tupla `tup = (1,2,3,4)`. ¿Qué se obtiene al hacer `tup[0] = 5`?

  1. ¿Cuántos elementos tiene la siguiente tupla: `tup_b = (10,)`? ¿Qué sucede si se omite la coma?
